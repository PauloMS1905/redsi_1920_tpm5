use Gerenciador_Torneios; # Usa base de dados pretendida
create database Gerenciador_Torneios; # Cria base de dados.

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: 'Gerenciador_Torneios'
--

-- --------------------------------------------------------

--
-- Estrutura da tabela 'atleta'

CREATE TABLE `atleta` (  # Cria tabela e posteriormente adiciona as colunas da tabela.
  `IDAtleta` int(11) NOT NULL,
  `nomeAtleta` varchar(100) DEFAULT NULL,
  `idade` int(11) DEFAULT NULL,
  `nacionalidade` varchar(50) DEFAULT NULL,
  `curiosidadesAtleta` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `atleta`
--

INSERT INTO `atleta` (`IDAtleta`, `nomeAtleta`, `idade`, `nacionalidade`, `curiosidadesAtleta`) VALUES
(1, 'Jackson Martínez', 31, 'COL', 'Ponta-de_lança matador, com o faro apurado para o golo'),
(2, 'Jesus Del Campo', 42, 'ESP', 'Última temporada no ativo como atleta profissional'),
(10, 'Nakajima', 22, 'JAPAO','Antes da carreira profissional como atleta, sonhava em ser padre'),
(7, 'Ricardo Quaresma', 33, 'PT', 'Realização de serviço comunitário, após ser intercetado pela venda ilicita de estupefacientes'),
(9, 'Ronaldinho Gaucho', 38, 'BR', 'Após 8 golos numa só partida de futebol amador, foi recrutado para ingressar no Flamengo FC'),
(19, 'Neymar Jr', 25, 'BR', 'Habitual participante no carnaval do Rio de Janeiro'),
(99, 'Vitor Baía', 46, 'PT', 'Vencedor do melhor guarda-redes do mundo em 2005'),
(78, 'Luis Vaz de Camões', 62, 'PT', 'A idade é só um numero');

-- --------------------------------------------------------
--
-- Estrutura da tabela 'equipa'
--
CREATE TABLE `equipa` (  # Cria tabela e posteriormente adiciona as colunas da tabela.
  `IDEquipa` int(11) NOT NULL,
  `nomeEquipa` varchar(100) DEFAULT NULL,
  `modalidade` varchar(100) DEFAULT NULL,
  `treinador` varchar(100) DEFAULT NULL,
  `recintoDesportivo` varchar(100) DEFAULT NULL,
  `dataConstrucao` date DEFAULT NULL,
  `lotacao` int(200) DEFAULT NULL,
  `curiosidadesEquipas` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `equipa`
--

INSERT INTO `equipa` (`IDEquipa`, `nomeEquipa`, `modalidade`, `treinador`, `recintoDesportivo`, `dataConstrucao`,`lotacao`, `curiosidadesEquipas`) VALUES
(1, 'Wolverhampton Wanderers Football Club', 'Futebol', 'Nuno Espírito Santo', 'Molineux Stadium', '1901-05-09', '65210', 'O próprio nome Wolves troveja nas páginas da história do futebol inglês'),
(8, 'Juventus Football Club', 'Futebol', 'Maurizio Sarri', 'Juventus Stadium', '1992-02-10', '83241', 'A maior detentora de scudettos do futebol Italiano com 35 títulos internos' ),
(10, 'Amsterdamsche Football Club Ajax', 'Futebol', ' Erik ten Hag',' Amsterdam Arena','1900-05-18', '54990','Ajax entrou no Guinness World Records como o clube com a maior sequência de vitórias consecutivas em jogos oficiais do futebol mundial'),
(7, 'Borussia Dortmund', 'Futebol', 'Lucien Favre', 'Signal Iduna Park', '1909-12-19', '81359', 'Em 1949, o Borussia chegou à sua primeira final em Stuttgart contra o VfR Mannheim mas perderam por 3-2 na prorrogação'),
(9, 'Liverpool Football Club', 'Futebol', 'Jürgen Klopp', ' Anfield', '1892-06-03', '53394' , 'O lema do clube é You will Never Walk Alone'),
(45, 'RasenBallsport Leipzig', 'Futebol', 'Julian Nagelsmann', 'Red Bull Arena Leipzig' ,'2009-05-19', '42500', 'Patrocinada pela multinacional austríaca de bebidas energéticas Red Bull'),
(18, 'Paris Saint-Germain Football Club','Futebol', 'Thomas Tuchel', 'Estádio Parc des Princes', '1970-08-12', '48583', 'Em 2011, a equipa foi comprada pela QSI (Qatar Sports Investment), um fundo de investimentos ligado ao governo do Catar'),
(79, 'Association Sportive de Monaco Football Club', 'Futebol', 'Robert Moreno', 'Louis II', '1924-08-23', '18523', 'Les Rouge et Blanc como são conhecidos'),
(25, 'Futebol Clube do Porto', 'Futebol', 'Sérgio Conceição', 'Estádio do Dragão', '2003-09-28', '50035', 'É o clube mais bem sucedido de Portugal com sete títulos internacionais'),
(14,'AC Milan', 'Futebol', ' Stefano Pioli', 'San Siro', '1899-12-16', '80018','Diavolo (The Devil) como são apelidados'),
(29, 'Chelsea Football Club', 'Futebol', 'Frank Lampard', 'Stamford Bridge', '1905-03-10', '41663', 'Entre 1939 e 1945, o Chelsea foi forçado a abandonar a Liga Inglesa, devido à Segunda Guerra Mundial'),
(35, 'Arsenal Football Club', 'Futebol', 'Mikel Arteta', 'Stadium Emirates', '1913-05-23', '60704', 'O clube é um dos mais bem sucedidos do futebol inglês, tendo ganhado por 13 vezes o título de campeão do Campeonato Inglês'),
(62, 'Futbol Club Barcelona', 'Futebol', 'Quique Setién', 'Camp Nou', '1899-11-29', '99789', 'O Barcelona é o segundo clube de futebol mais valioso do mundo'),
(81, 'Tottenham Hotspur Football Club','Futebol', 'José Mourinho', 'Tottenham Hotspur Stadium', '2012-09-05', '62062','Foi fundado em 1882, originário de um clube de cricket e se chamava Hotspur FC');
-- --------------------------------------------------------
--
-- Estrutura da tabela 'campeonato'
--
CREATE TABLE `campeonato` (  # Cria tabela e posteriormente adiciona as colunas da tabela.
  `IDCampeonato` int(11) NOT NULL,
  `nomeCampeonato` varchar(100) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL,
  `dataCriacao` date DEFAULT NULL,
  `vencedor` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `campeonato` (`IDCampeonato`, `nomeCampeonato`, `estado`, `dataCriacao`, `vencedor`) VALUES
(9, 'League Achille & Cesare Bortolotti', 'A decorrer', '2019-09-20', ''),
(12, 'League Sanwa Bank', 'A decorrer', '2020-06-23', ''),
(39, 'League Ramón de Carranza', 'A decorrer', '2020-12-16', ''),
(32, 'League Nippon Ham', 'Terminado', '2019-03-01', 'Futebol Clube do Porto'),
(21, 'League Akwaba', 'Terminado', '2019-04-25', 'Amsterdamsche Football Club Ajax'),
(14, 'League Pierre Colon', 'A decorrer', '2020-01-05', ''),
(15, 'League Jubileu de Ouro AFLP', 'A decorrer', '2020-03-19', ''),
(98, 'League K1', 'Terminado', '2019-08-20', 'RasenBallsport Leipzig');

CREATE TABLE `eliminatoria` (  # Cria tabela e posteriormente adiciona as colunas da tabela.
  `IDEliminatoria` int(11) NOT NULL,
  `nomeEliminatoria` varchar(100) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL,
  `dataCriacao` date DEFAULT NULL,
  `vencedor` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `eliminatoria` (`IDEliminatoria`, `nomeEliminatoria`, `estado`, `dataCriacao`, `vencedor`) VALUES
(53, 'Super Cup Pioneer', 'A decorrer', '2019-12-04', ''),
(97, 'Cup Master Kaiser Afasc', 'A decorrer', '2020-01-26', ''),
(78, 'TFF Süper Kupa', 'Terminado', '2019-03-14', 'Borussia Dortmund'),
(89, 'Community Shield', 'Terminado', '2019-01-09', 'Association Sportive de Monaco Football Club'),
(29, 'Johan Cruijff Schaal', 'Terminado', '2019-11-19', 'Tottenham Hotspur Football Club'),
(19, 'Welsh PL Championship round', 'A decorrer', '2019-08-09', ''),
(16, 'Coupe de la Ligue', 'A decorrer', '2020-02-19', ''),
(30, 'Kasachischer Supercup', 'Terminado', '2019-06-21', 'Arsenal Football Club');

CREATE TABLE `eliminatoriaGrupo` (  # Cria tabela e posteriormente adiciona as colunas da tabela.
  `IDEliminatoriaGrupo` int(11) NOT NULL,
  `nomeEliminatoriaGrupo` varchar(100) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL,
  `dataCriacao` date DEFAULT NULL,
  `vencedor` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `eliminatoriaGrupo` (`IDEliminatoriaGrupo`, `nomeEliminatoriaGrupo`, `estado`, `dataCriacao`, `vencedor`) VALUES
(57, 'Aphrodite Cup League', 'A decorrer', '2019-09-11', ''),
(89, 'Bangabandhu Cup League', 'A decorrer', '2019-10-20', ''),
(91, 'Mjólkurbikar Cup qualifiers', 'Terminado', '2019-04-21', 'AC Milan'),
(96, 'Kubok Kazakhstana', 'Terminado', '2019-05-18', 'Paris Saint-Germain Football Club'),
(37, 'Hrvatski nogometni kup', 'Terminado', '2020-01-07', 'Liverpool Football Club'),
(29, 'TOTO KNVB beker', 'A decorrer', '2019-05-30', ''),
(18, 'Beker van Belgie', 'A decorrer', '2020-01-19', ''),
(39, 'Sydbank Pokalen', 'Terminado', '2019-10-12', 'Juventus Football Club');

CREATE TABLE `equipaAtleta` (
  `IDEquipa` int(11) NOT NULL,
  `IDAtleta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `equipaAtleta` (`IDEquipa`, `IDAtleta`) VALUES (1, 1),(45, 2),(79, 10),(14, 7),(81, 9),(35, 19),(62, 99),(29, 78);

CREATE TABLE `equipaCampeonato` (
  `IDCampeonato` int(11) NOT NULL,
  `IDEquipa` int(11) NOT NULL,
  `classificacao` int (11) NOT NULL,
  `pontuacao` int (11) NOT NULL,
  `vitorias` int (11) NOT NULL,
  `derrotas` int (11) NOT NULL,
  `empates` int (11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `equipaCampeonato` (`IDCampeonato`, `IDEquipa`, `classificacao`, `pontuacao`, `vitorias`, `derrotas`, `empates`) VALUES
(9, 1, 3, 45, 13, 4, 6),
(12, 8, 1, 75, 24, 0, 3),
(39, 10, 1, 30, 10, 0, 0),
(32, 7, 2, 41, 12, 3, 5),
(21, 9, 9, 12, 2, 3, 3 ),
(14, 45, 7, 21, 5, 6, 5),
(15, 18, 10, 0, 0, 2, 0),
(98, 79, 5, 32, 8, 9, 8),
(39, 25, 2, 43, 13, 6, 4),
(14, 14, 4, 66, 21, 3, 3),
(98, 29, 2, 40, 10, 5, 10),
(15, 35, 1, 6, 2, 0, 0),
(32, 62, 1, 42, 13, 4 , 3),
(12, 81, 2, 74, 24, 1, 2);

CREATE TABLE `equipaEliminatoria` (
  `IDEliminatoria` int(11) NOT NULL,
  `IDEquipa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `equipaEliminatoria` (`IDEliminatoria`, `IDEquipa`) VALUES (53, 1),(97, 8),(78, 10),(89, 7),(29, 9),(19, 29),(16, 35),(30, 79);

CREATE TABLE `equipaEliminatoriaGrupo` (
  `IDEliminatoriaGrupo` int(10) NOT NULL,
  `IDEquipa` int(11) NOT NULL,
  `classificacao` int (11) NOT NULL,
  `pontuacao` int (11) NOT NULL,
  `vitorias` int (11) NOT NULL,
  `derrotas` int (11) NOT NULL,
  `empates` int (11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `equipaEliminatoriaGrupo` (`IDEliminatoriaGrupo`, `IDEquipa`, `classificacao`, `pontuacao`, `vitorias`, `derrotas`, `empates`) VALUES
(57, 62, 1, 9, 3, 0, 0),
(89, 81, 2, 7, 2, 0, 1),
(91, 10, 4, 1, 0, 2, 1),
(96, 7, 3, 2, 0, 0, 2),
(37, 14, 1, 6, 2, 0, 0 ),
(29, 18, 4, 0, 0, 0, 0),
(18, 9, 3, 0, 0, 0, 2),
(39, 25, 1, 6, 2, 0, 0 );

CREATE TABLE `jogos` (
  `IDJogo` int (11) NOT NULL,
  `IDEquipa1` int(11) NOT NULL,
  `IDEquipa2` int(11) NOT NULL,
  `IDTipoTorneio` int (11) NOT NULL,
  `resultado` varchar(100) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `jogos` (`IDJogo`,`IDEquipa1`, `IDEquipa2`, `IDTipoTorneio`, `resultado`, `data`) VALUES
(1, 29, 79, 2, '3-1', '2020-02-04'),
(2, 18, 35, 3, '4-3', '2020-01-02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipoCampeonato`
--

CREATE TABLE `tipoTorneio` (
  `IDTipoTorneio` int(11) NOT NULL,
  `designacao` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `tipoTorneio` (`IDTipoTorneio`, `designacao`) VALUES
(1, 'Campeonato'),
(2, 'Eliminatoria'),
(3, 'EliminatoriaGrupo');

--
-- Alterações na tabela 'tipoTorneio'
--

ALTER TABLE `tipoTorneio`
  ADD PRIMARY KEY (`IDTipoTorneio`);
  
ALTER TABLE `tipoTorneio`
  MODIFY `IDTipoTorneio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
  
--
-- Alterações na tabela 'atleta'
--

ALTER TABLE `atleta`
  ADD PRIMARY KEY (`IDAtleta`);

ALTER TABLE `atleta`
  MODIFY `IDAtleta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
  
--
-- Alterações na tabela 'equipa'
--

ALTER TABLE `equipa`
	ADD PRIMARY KEY (`IDEquipa`);

ALTER TABLE `equipa`
  MODIFY `IDEquipa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
  
--
-- Alterações na tabela 'equipaAtleta'
--

ALTER TABLE `equipaAtleta`
  ADD PRIMARY KEY (`IDEquipa`,`IDAtleta`),
  ADD KEY `IDAtleta` (`IDAtleta`);

ALTER TABLE `equipaAtleta`
  ADD CONSTRAINT `equipaAtleta_ibfk_1` FOREIGN KEY (`IDEquipa`) REFERENCES `equipa` (`IDEquipa`),
  ADD CONSTRAINT `equipaAtleta_ibfk_2` FOREIGN KEY (`IDAtleta`) REFERENCES `atleta` (`IDAtleta`);

--
-- Alterações na tabela 'campeonato'
--

ALTER TABLE `campeonato`
  ADD PRIMARY KEY (`IDCampeonato`);

ALTER TABLE `campeonato`
  MODIFY `IDCampeonato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Alterações na tabela 'eliminatoria'
--

ALTER TABLE `eliminatoria`
  ADD PRIMARY KEY (`IDEliminatoria`);
  
ALTER TABLE `eliminatoria`
  MODIFY `IDEliminatoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Alterações na tabela 'eliminatoriaGrupo'
--

ALTER TABLE `equipaEliminatoria`
  ADD PRIMARY KEY (`IDEliminatoriaGrupo`);

ALTER TABLE `eliminatoriaGrupo`
  MODIFY `IDEliminatoriaGrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Alterações na tabela 'equipaCampeonato'
--
ALTER TABLE `equipaCampeonato`
  ADD PRIMARY KEY (`IDEquipa`,`IDCampeonato`),
  ADD KEY `IDEquipa` (`IDEquipa`);

  ALTER TABLE `equipaCampeonato`
  ADD CONSTRAINT `equipaCampeonato_ibfk_1` FOREIGN KEY (`IDEquipa`) REFERENCES `equipa` (`IDEquipa`),
  ADD CONSTRAINT `equipaCampeonato_ibfk_2` FOREIGN KEY (`IDCampeonato`) REFERENCES `campeonato` (`IDCampeonato`);

--
-- Alterações na tabela 'equipaEliminatoria'
--
ALTER TABLE `equipaEliminatoria`
  ADD PRIMARY KEY (`IDEquipa`,`IDEliminatoria`),
  ADD KEY `IDEquipa` (`IDEquipa`);

  ALTER TABLE `equipaEliminatoria`
  ADD CONSTRAINT `equipaEliminatoria_ibfk_1` FOREIGN KEY (`IDEquipa`) REFERENCES `equipa` (`IDEquipa`),
  ADD CONSTRAINT `equipaEliminatoria_ibfk_2` FOREIGN KEY (`IDEliminatoria`) REFERENCES `eliminatoria` (`IDEliminatoria`);

--
-- Alterações na tabela 'equipaEliminatoriaGrupo'
--

ALTER TABLE `equipaEliminatoriaGrupo`
  ADD PRIMARY KEY (`IDEquipa`,`IDEliminatoriaGrupo`),
  ADD KEY `IDEquipa` (`IDEquipa`);

ALTER TABLE `equipaEliminatoriaGrupo`
  ADD CONSTRAINT `equipaEliminatoriaGrupo_ibfk_1` FOREIGN KEY (`IDEquipa`) REFERENCES `equipa` (`IDEquipa`),
  ADD CONSTRAINT `equipaEliminatoriaGrupo_ibfk_2` FOREIGN KEY (`IDEliminatoriaGrupo`) REFERENCES `eliminatoriaGrupo` (`IDEliminatoriaGrupo`);
  
--
-- Alterações na tabela 'Jogos'
--

ALTER TABLE `jogos`
  ADD PRIMARY KEY (`IDJogo`),
  ADD KEY `IDTipoTorneio` (`IDTipoTorneio`);

ALTER TABLE `jogos`
  MODIFY `IDJogo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
  
ALTER TABLE `jogos`
  ADD CONSTRAINT `jogos_ibfk_1` FOREIGN KEY (`IDEquipa1`) REFERENCES `equipa` (`IDEquipa`),
  ADD CONSTRAINT `jogos_ibfk_2` FOREIGN KEY (`IDEquipa2`) REFERENCES `equipa` (`IDEquipa`),
  ADD CONSTRAINT `jogos_ibfk_3` FOREIGN KEY (`IDTipoTorneio`) REFERENCES `tipoTorneio` (`IDTipoTorneio`);
  
  
  
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;