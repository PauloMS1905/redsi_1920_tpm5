/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_WEB;

import Gerenciador_Torneios_Controller.AtletaController;
import Gerenciador_Torneios_Controller.CampeonatoController;
import Gerenciador_Torneios_Controller.EliminatoriaController;
import Gerenciador_Torneios_Controller.EliminatoriaGrupoController;
import Gerenciador_Torneios_Controller.EquipaAtletaController;
import Gerenciador_Torneios_Controller.EquipaCampeonatoController;
import Gerenciador_Torneios_Controller.EquipaController;
import Gerenciador_Torneios_Controller.EquipaEliminatoriaController;
import Gerenciador_Torneios_Controller.EquipaEliminatoriaGrupoController;
import Gerenciador_Torneios_Controller.JogosController;
import Gerenciador_Torneios_Model.Atleta;
import Gerenciador_Torneios_Model.Campeonato;
import Gerenciador_Torneios_Model.Eliminatorias;
import Gerenciador_Torneios_Model.Equipa;
import Gerenciador_Torneios_Model.EquipaAtleta;
import Gerenciador_Torneios_Model.EquipaCampeonato;
import Gerenciador_Torneios_Model.EquipaEliminatoria;
import Gerenciador_Torneios_Model.EquipaEliminatoriaGrupo;
import Gerenciador_Torneios_Model.GruposEliminar;
import Gerenciador_Torneios_Model.Jogos;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WebServer extends AbstractVerticle {

    @Override
    public void start(Future<Void> fut) throws IOException {

        // Criar objecto router, 
        // Encaminhara´ os pedidos, de acordo com caminho no URL
        Router router = Router.router(vertx);

        router.route("/").handler(StaticHandler.create("webroot/index.html"));

        router.route("/*").handler(StaticHandler.create("webroot")); //usa index.html
        router.route("/atletas/").handler(StaticHandler.create("webroot/GestaoAtletas.html"));
        router.route("/equipas/").handler(StaticHandler.create("webroot/GestaoEquipas.html"));
        router.route("/eliminatorias/").handler(StaticHandler.create("webroot/GestaoEliminatorias.html"));
        router.route("/gruposEliminatoria/").handler(StaticHandler.create("webroot/GestaoGruposEliminatorias.html"));
        router.route("/campeonatos/").handler(StaticHandler.create("webroot/GestaoCampeoanto.html"));
        router.route("/jogos/").handler(StaticHandler.create("webroot/Jogos.html"));

        // Atleta 
        router.get("/atletas/:num").handler(this::getAtleta);
        router.post("/atletas/add*").handler(BodyHandler.create());
        router.post("/atletas/add").handler(this::addAtleta);

        // Equipa
        router.get("/equipas/:num").handler(this::getEquipa);
        router.post("/equipas/add*").handler(BodyHandler.create());
        router.post("/equipas/add").handler(this::addEquipa);

        // EquipaAtleta
        router.post("/atleta-equipa/associar*").handler(BodyHandler.create());
        router.post("/atleta-equipa/associar").handler(this::associarEquipaAtleta);

        // Campeonato
        router.get("/campeonato/:num").handler(this::getCampeonato);
        router.post("/campeonato/add*").handler(BodyHandler.create());
        router.post("/campeonato/add").handler(this::addCampeonato);

        // Eliminatoria
        router.get("/eliminatorias/:num").handler(this::getEliminatoria);
        router.post("/eliminatorias/add*").handler(BodyHandler.create());
        router.post("/eliminatorias/add").handler(this::addEliminatoria);

        // EliminatoriaGrupo
        router.get("/gruposEliminatoria/:num").handler(this::getEliminatoriaGrupo);
        router.post("/gruposEliminatoria/add*").handler(BodyHandler.create());
        router.post("/gruposEliminatoria/add").handler(this::addEliminatoriaGrupo);

        // EquipaCampeonato
        router.post("/campeonato-equipa/associar*").handler(BodyHandler.create());
        router.post("/campeonato-equipa/associar").handler(this::associarEquipaCampeonato);

        // EquipaEliminatoria
        router.post("/eliminatoria/associar*").handler(BodyHandler.create());
        router.post("/eliminatoria/associar").handler(this::associarEquipaEliminatoria);

        // EquipaEliminatoriaGrupo     
        router.post("/gruposEliminatoria/associar*").handler(BodyHandler.create());
        router.post("/gruposEliminatoria/associar").handler(this::associarEquipaEliminatoriaGrupo);

        // Jogos   
        router.get("/jogos/:num").handler(this::getJogo);
        router.post("/jogos/add*").handler(BodyHandler.create());
        router.post("/jogos/add").handler(this::addJogo);

        // Cria o servidor HTTP e passa o método accept() ao handler do pedido (request handler).
        vertx.createHttpServer().requestHandler(router::accept).listen(8080, (AsyncResult<HttpServer> result) -> {
            if (result.succeeded()) {
                fut.complete(); //concretiza criacao
            } else {
                fut.fail(result.cause()); // nao conseguiu criar o servidor
                System.err.println("Couldn't connect...");
            }
        });
    }

    private void getAtleta(RoutingContext routingContext) {
        System.out.println("showAtleta() - " + routingContext.toString());
        String numero = routingContext.request().getParam("num");
        System.out.println("numero: " + numero);
        try {
            int num = Integer.valueOf(numero);

            // E obtem atleta com esse num
            Atleta atleta = new Atleta();
            AtletaController.mostrarAtleta(num, atleta);
            System.out.println("atleta obtido: " + atleta.getNomeAtleta());
            // Se atleta nao existe 
            //    routingContext.response().setStatusCode(404).end(); // recurso não encontrado
            // senao 
            routingContext.response()
                    .setStatusCode(200) // ok e devolve entidade solicitada
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(atleta));
        } catch (NumberFormatException e) {
            routingContext.response().setStatusCode(400).end();
        } catch (SQLException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addAtleta(RoutingContext routingContext) {
        System.out.println("addAtleta() - " + routingContext.toString());
        try {
            JsonObject body = routingContext.getBodyAsJson();

            Atleta atleta = body.mapTo(Atleta.class);
            // aqui adicionar atleta
            System.out.println(atleta.getNacionalidade());
            System.out.println("atleta " + atleta.getNomeAtleta() + " adicionado!");
            AtletaController.guardarAtleta(atleta);
            // responde com o novo atleta
            routingContext.response()
                    .setStatusCode(201) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(atleta));
        } catch (Exception e) {
            System.out.println("exception: " + e.getMessage());

            routingContext.response()
                    .setStatusCode(500) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodeToBuffer("{erro: 'erro!'}"));
        }
    }

    private void associarEquipaAtleta(RoutingContext routingContext) {
        System.out.println("associarEquipa() - " + routingContext.toString());
        try {
            JsonObject body = routingContext.getBodyAsJson();

            EquipaAtleta equipaAtleta = body.mapTo(EquipaAtleta.class);

            System.out.println("Equipa: " + equipaAtleta.getidEquipa() + " adicionado!");
            EquipaAtletaController.associarEquipaAtleta(equipaAtleta);

            routingContext.response()
                    .setStatusCode(201) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(equipaAtleta));
        } catch (Exception e) {
            System.out.println("exception: " + e.getMessage());

            routingContext.response()
                    .setStatusCode(500) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodeToBuffer("{erro: 'erro!'}"));
        }
    }

    private void getEquipa(RoutingContext routingContext) {
        System.out.println("showEquipa() - " + routingContext.toString());
        String numero = routingContext.request().getParam("num");
        System.out.println("numero: " + numero);
        try {
            int num = Integer.valueOf(numero);

            // E obtem equipa com esse num
            Equipa equipa = new Equipa();
            EquipaController.mostrarEquipa(num, equipa);
            System.out.println("equipa obtida: " + equipa.getNomeEquipa());
            // Se equipa nao existe 
            //    routingContext.response().setStatusCode(404).end(); // recurso não encontrado
            // senao 
            routingContext.response()
                    .setStatusCode(200) // ok e devolve entidade solicitada
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(equipa));
        } catch (NumberFormatException e) {
            routingContext.response().setStatusCode(400).end();
        } catch (SQLException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addEquipa(RoutingContext routingContext) {
        System.out.println("addEquipa() - " + routingContext.toString());
        try {
            JsonObject body = routingContext.getBodyAsJson();

            Equipa equipa = body.mapTo(Equipa.class);
            // aqui adicionar equipa
            System.out.println("equipa " + equipa.getNomeEquipa() + " adicionada!");
            EquipaController.guardarEquipa(equipa);
            // responde com a nova equipa
            routingContext.response()
                    .setStatusCode(201) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(equipa));
        } catch (Exception e) {
            System.out.println("exception: " + e.getMessage());

            routingContext.response()
                    .setStatusCode(500) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodeToBuffer("{erro: 'erro!'}"));
        }
    }

    private void addCampeonato(RoutingContext routingContext) {
        System.out.println("addCampeonato() - " + routingContext.toString());
        try {
            JsonObject body = routingContext.getBodyAsJson();

            Campeonato campeonato = body.mapTo(Campeonato.class);
            // aqui adicionar campeonato
            System.out.println("campeonato " + campeonato.getNomeCampeonato() + " adicionado!");
            CampeonatoController.guardarCampeonato(campeonato);
            // responde com o novo campeonato
            routingContext.response()
                    .setStatusCode(201) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(campeonato));
        } catch (Exception e) {
            System.out.println("exception: " + e.getMessage());

            routingContext.response()
                    .setStatusCode(500) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodeToBuffer("{erro: 'erro!'}"));
        }
    }

    private void getCampeonato(RoutingContext routingContext) {
        System.out.println("showCampeonato() - " + routingContext.toString());
        String numero = routingContext.request().getParam("num");
        System.out.println("numero: " + numero);
        try {
            int num = Integer.valueOf(numero);

            // E obtem atleta com esse num
            Campeonato campeonato = new Campeonato();
            CampeonatoController.mostrarCampeonato(num, campeonato);
            System.out.println("campeonato obtido: " + campeonato.getNomeCampeonato());
            // Se atleta nao existe 
            //    routingContext.response().setStatusCode(404).end(); // recurso não encontrado
            // senao 
            routingContext.response()
                    .setStatusCode(200) // ok e devolve entidade solicitada
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(campeonato));
        } catch (NumberFormatException e) {
            routingContext.response().setStatusCode(400).end();
        } catch (SQLException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addEliminatoria(RoutingContext routingContext) {
        System.out.println("addEliminatoria() - " + routingContext.toString());
        try {
            JsonObject body = routingContext.getBodyAsJson();

            Eliminatorias eliminatoria = body.mapTo(Eliminatorias.class);
            // aqui adicionar eliminatoria
            System.out.println("eliminatoria " + eliminatoria.getNomeEliminatoria() + " adicionado!");
            EliminatoriaController.guardarEliminatoria(eliminatoria);
            // responde com a nova eliminatoria
            routingContext.response()
                    .setStatusCode(201) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(eliminatoria));
        } catch (Exception e) {
            System.out.println("exception: " + e.getMessage());

            routingContext.response()
                    .setStatusCode(500) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodeToBuffer("{erro: 'erro!'}"));
        }
    }

    private void getEliminatoria(RoutingContext routingContext) {
        System.out.println("showEliminatoria() - " + routingContext.toString());
        String numero = routingContext.request().getParam("num");
        System.out.println("numero: " + numero);
        try {
            int num = Integer.valueOf(numero);

            // E obtem eliminatoria com esse num
            Eliminatorias eliminatoria = new Eliminatorias();
            EliminatoriaController.mostrarEliminatoria(num, eliminatoria);
            System.out.println("eliminatoria obtida: " + eliminatoria.getNomeEliminatoria());
            // Se eliminatoria nao existe 
            //    routingContext.response().setStatusCode(404).end(); // recurso não encontrado
            // senao 
            routingContext.response()
                    .setStatusCode(200) // ok e devolve entidade solicitada
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(eliminatoria));
        } catch (NumberFormatException e) {
            routingContext.response().setStatusCode(400).end();
        } catch (SQLException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addEliminatoriaGrupo(RoutingContext routingContext) {
        System.out.println("addEliminatoriaGrupo() - " + routingContext.toString());
        try {
            JsonObject body = routingContext.getBodyAsJson();

            GruposEliminar eliminatoriaGrupos = body.mapTo(GruposEliminar.class);
            // aqui adicionar eliminatoriaGrupo
            System.out.println("eliminatoriaGrupos " + eliminatoriaGrupos.getNomeGruposEliminatorias() + " adicionado!");
            EliminatoriaGrupoController.guardarEliminatoriaGrupo(eliminatoriaGrupos);
            // responde com a nova eliminatoria
            routingContext.response()
                    .setStatusCode(201) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(eliminatoriaGrupos));
        } catch (Exception e) {
            System.out.println("exception: " + e.getMessage());

            routingContext.response()
                    .setStatusCode(500) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodeToBuffer("{erro: 'erro!'}"));
        }
    }

    private void getEliminatoriaGrupo(RoutingContext routingContext) {
        System.out.println("showEliminatoriaGrupo() - " + routingContext.toString());
        String numero = routingContext.request().getParam("num");
        System.out.println("numero: " + numero);
        try {
            int num = Integer.valueOf(numero);

            // E obtem eliminatoriaGrupo com esse num
            GruposEliminar eliminatoriaGrupos = new GruposEliminar();
            EliminatoriaGrupoController.mostrarEliminatoriaGrupos(num, eliminatoriaGrupos);
            System.out.println("eliminatoriaGrupo obtida: " + eliminatoriaGrupos.getNomeGruposEliminatorias());
            // Se eliminatoriaGrupo nao existe 
            //    routingContext.response().setStatusCode(404).end(); // recurso não encontrado
            // senao 
            routingContext.response()
                    .setStatusCode(200) // ok e devolve entidade solicitada
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(eliminatoriaGrupos));
        } catch (NumberFormatException e) {
            routingContext.response().setStatusCode(400).end();
        } catch (SQLException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void associarEquipaCampeonato(RoutingContext routingContext) {
        System.out.println("associarCampeonato() - " + routingContext.toString());
        try {
            JsonObject body = routingContext.getBodyAsJson();

            EquipaCampeonato equipaCampeonato = body.mapTo(EquipaCampeonato.class);

            System.out.println("Campeonato: " + equipaCampeonato.getidCampeonato() + " adicionado!");
            EquipaCampeonatoController.associarEquipaCampeonato(equipaCampeonato);

            routingContext.response()
                    .setStatusCode(201) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(equipaCampeonato));
        } catch (Exception e) {
            System.out.println("exception: " + e.getMessage());

            routingContext.response()
                    .setStatusCode(500) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodeToBuffer("{erro: 'erro!'}"));
        }
    }

    private void associarEquipaEliminatoria(RoutingContext routingContext) {
        System.out.println("associarEliminatoria() - " + routingContext.toString());
        try {
            JsonObject body = routingContext.getBodyAsJson();

            EquipaEliminatoria equipaEliminatoria = body.mapTo(EquipaEliminatoria.class);

            System.out.println("Eliminatoria: " + equipaEliminatoria.getidEliminatoria() + " adicionado!");
            EquipaEliminatoriaController.associarEquipaEliminatoria(equipaEliminatoria);

            routingContext.response()
                    .setStatusCode(201) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(equipaEliminatoria));
        } catch (Exception e) {
            System.out.println("exception: " + e.getMessage());

            routingContext.response()
                    .setStatusCode(500) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodeToBuffer("{erro: 'erro!'}"));
        }
    }

    private void associarEquipaEliminatoriaGrupo(RoutingContext routingContext) {
        System.out.println("associarEliminatoriaGrupo() - " + routingContext.toString());
        try {
            JsonObject body = routingContext.getBodyAsJson();

            EquipaEliminatoriaGrupo equipaEliminatoriaGrupo = body.mapTo(EquipaEliminatoriaGrupo.class);

            System.out.println("EliminatoriaGrupo: " + equipaEliminatoriaGrupo.getidEliminatoriaGrupo() + " adicionado!");
            EquipaEliminatoriaGrupoController.associarEquipaEliminatoriaGrupo(equipaEliminatoriaGrupo);

            routingContext.response()
                    .setStatusCode(201) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(equipaEliminatoriaGrupo));
        } catch (Exception e) {
            System.out.println("exception: " + e.getMessage());

            routingContext.response()
                    .setStatusCode(500) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodeToBuffer("{erro: 'erro!'}"));
        }
    }

    private void getJogo(RoutingContext routingContext) {
        System.out.println("showJogo() - " + routingContext.toString());
        String numero = routingContext.request().getParam("num");
        System.out.println("numero: " + numero);
        try {
            int num = Integer.valueOf(numero);

            // E obtem jogo com esse num
            Jogos jogo = new Jogos();
            JogosController.mostrarJogo(num, jogo);
            System.out.println("jogo obtido: " + jogo.getidEquipa1() + jogo.getidEquipa2() + jogo.getResultado());
            // Se jogo nao existe 
            //    routingContext.response().setStatusCode(404).end(); // recurso não encontrado
            // senao 
            routingContext.response()
                    .setStatusCode(200) // ok e devolve entidade solicitada
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(jogo));
        } catch (NumberFormatException e) {
            routingContext.response().setStatusCode(400).end();
        } catch (SQLException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addJogo(RoutingContext routingContext) {
        System.out.println("addJogo() - " + routingContext.toString());
        try {
            JsonObject body = routingContext.getBodyAsJson();

            Jogos jogo = body.mapTo(Jogos.class);
            // aqui adicionar eliminatoria
            System.out.println("jogo " + jogo.getidJogo() + " adicionado!");
            JogosController.guardarJogo(jogo);
            // responde com o novo jogo
            routingContext.response()
                    .setStatusCode(201) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(jogo));
        } catch (Exception e) {
            System.out.println("exception: " + e.getMessage());

            routingContext.response()
                    .setStatusCode(500) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodeToBuffer("{erro: 'erro!'}"));
        }
    }
}
