package Gerenciador_Torneios_DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBFactory {

    static final String URL = "jdbc:mysql://localhost:3306/Gerenciador_Torneios";
    static final String USER = "root";
    static final String PASS = "";

    public static Connection getConnection() {
        try {
            Connection conn = DriverManager.getConnection(URL, USER, PASS);
            System.out.println(String.format("Connection to %s succeeded!", conn.getCatalog()));

            return conn;
        } catch (SQLException exc) {
            throw new RuntimeException("!!ERROR CONNECTING!!", exc);
        }
    }

}
