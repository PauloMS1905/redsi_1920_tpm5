/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_DB;

import Gerenciador_Torneios_Model.EquipaAtleta;
import Gerenciador_Torneios_Model.Atleta;
import Gerenciador_Torneios_Model.Campeonato;
import Gerenciador_Torneios_Model.Eliminatorias;
import Gerenciador_Torneios_Model.Equipa;
import Gerenciador_Torneios_Model.EquipaCampeonato;
import Gerenciador_Torneios_Model.EquipaEliminatoria;
import Gerenciador_Torneios_Model.EquipaEliminatoriaGrupo;
import Gerenciador_Torneios_Model.GruposEliminar;
import Gerenciador_Torneios_Model.Jogos;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author utilizador
 */
public class DAL {

    public static void showAtleta(int numero, Atleta a) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select * from atleta where IDAtleta=" + numero + ";");
        try {
            rs.next();
            String aux = rs.getString("nomeAtleta");
            String cur = rs.getString("curiosidadesAtleta");
            a.setNomeAtleta(aux);
            a.setNumeroIdentificacaoAtleta(numero);
            a.setCuriosidadesAtleta(cur);
            a.setIdade(Integer.parseInt(rs.getString("idade")));
            a.setNacionalidade(rs.getString("nacionalidade"));
        } catch (SQLException ex) {
            System.out.println("erro get: " + ex.getStackTrace());
        }
        conn.close();
    }

    public static boolean addAtleta(Atleta a) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into atleta(IDAtleta,nomeAtleta,idade,nacionalidade,curiosidadesAtleta) values(" + a.getNumeroIdentifacaoAtleta() + ",'" + a.getNomeAtleta() + "'," + a.getIdade() + ",'"
                + a.getNacionalidade() + "'," + a.getCuriosidadesAtleta() + ");");
        conn.close();
        return res;
    }

    public static boolean addEquipaAtleta(EquipaAtleta c) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into equipaAtleta(IDEquipa,IDAtleta) values("
                + c.getidEquipa() + "," + c.getidAtleta() + ");");
        conn.close();
        return res;
    }

    public static void showEquipa(int numero, Equipa e) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select * from equipa where IDEquipa=" + numero + ";");
        try {
            rs.next();
            String aux = rs.getString("nome");
            String mod = rs.getString("modalidade");
            e.setNomeEquipa(aux);
            e.setNumero(numero);
            e.setModalidade(mod);
            e.setLotacao(Integer.parseInt(rs.getString("lotação")));
            e.setRecintoDesportivo(rs.getString("recintoDesportivo"));
            e.setCuriosidadesEquipas(rs.getString("curiosidadesEquipa"));
        } catch (SQLException ex) {
            System.out.println("erro get: " + ex.getStackTrace());
        }
        conn.close();
    }

    public static boolean addEquipa(Equipa e) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into equipa(IDEquipa,nomeEquipa,modalidade,treinador,recintoDesportivo,dataConstrução, lotação, curiosidadesEquipas) values(" + e.getNumero() + "," + e.getNomeEquipa() + "," + e.getModalidade() + ","
                + e.getTreinador() + "," + e.getRecintoDesportivo() + "," + e.getDataContrucao() + "," + e.getLotacao() + "," + e.getCuriosidadesEquipas() + ");");
        conn.close();
        return res;
    }

    public static boolean addEquipaCampeonato(EquipaCampeonato c) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into equipaCampeonato(IDEquipa,IDCampeonato,classificação,pontuação,vitórias,derrotas, empates) values(" + c.getidEquipa() + "," + c.getidCampeonato() + "," + c.getClassificacao() + ","
                + c.getPontuacao() + "," + c.getVitorias() + "," + c.getDerrotas() + "," + c.getEmpates() + ");");
        conn.close();

        return res;
    }

    public static boolean addEquipaEliminatoria(EquipaEliminatoria el) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into equipaEliminatoria(IDEquipa,IDEliminatoria) values("
                + el.getidEquipa() + "," + el.getidEliminatoria() + ");");
        conn.close();
        return res;
    }

    public static boolean addEquipaEliminatoriaGrupo(EquipaEliminatoriaGrupo eg) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into equipaEliminatoriaGrupo(IDEquipa,IDEliminatoriaGrupo,classificação,pontuação,vitórias,derrotas,empates) values(" + eg.getidEquipa() + "," + eg.getidEliminatoriaGrupo() + "," + eg.getClassificacao() + ","
                + eg.getPontuacao() + "," + eg.getVitorias() + "," + eg.getDerrotas() + "," + eg.getEmpates() + ");");
        conn.close();
        return res;
    }

    public static void showCampeonato(int numero, Campeonato c) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select * from campeonato where IDCampeonato=" + numero + ";");
        try {
            rs.next();
            String aux = rs.getString("nome do campeonato");
            String state = rs.getString("estado");
            c.setNomeCampeonato(aux);
            c.setEstado(state);
            c.setNumeroCampeonato(numero);
            c.setVencedores(rs.getString("vencedores"));
            c.setidEquipas(Integer.parseInt(rs.getString("equipas")));
            c.setClassificacao(Integer.parseInt(rs.getString("classificação")));
            c.setPontuacao(Integer.parseInt(rs.getString("pontuação")));
            c.setVitorias(Integer.parseInt(rs.getString("vitórias")));
            c.setDerrotas(Integer.parseInt(rs.getString("derrotas")));
            c.setEmpates(Integer.parseInt(rs.getString("empates")));
        } catch (SQLException ex) {
            System.out.println("erro get: " + ex.getStackTrace());
        }
        conn.close();
    }

    public static boolean addCampeonato(Campeonato c) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into campeonato(IDCampeonato,nomeCampeonato,dataCriacao,estado,vencedores) values(" + c.getNumeroCampeonato() + "," + c.getNomeCampeonato() + ","
                + c.getDataCriacaoCampeonato() + "," + c.getEstado() + "," + c.getVencedores() + "," + ");");
        conn.close();
        return res;
    }

    public static void showEliminatoria(int numero, Eliminatorias e) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select * from eliminatoria where IDEliminatoria=" + numero + ";");
        try {
            rs.next();
            String aux = rs.getString("nome da eliminatoria");
            String state = rs.getString("estado");
            e.setNomeEliminatoria(aux);
            e.setEstado(state);
            e.setNumeroEliminatoria(numero);
            e.setVencedor(rs.getString("vencedor"));
            e.setidEquipas(Integer.parseInt(rs.getString("equipas")));
        } catch (SQLException ex) {
            System.out.println("erro get: " + ex.getStackTrace());
        }
        conn.close();
    }

    public static boolean addEliminatoria(Eliminatorias e) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into eliminatoria(IDEliminatoria,nomeEliminatoria,estado,dataCriacao,vencedores) values(" + e.getNumeroEliminatoria() + "," + e.getNomeEliminatoria() + ","
                + e.getEstado() + "," + e.getDataCriacaoEliminatoria() + "," + e.getVencedor() + "," + ");");
        conn.close();
        return res;
    }

    public static void showEliminatoriaGrupo(int numero, GruposEliminar eg) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select * from eliminatoriaGrupo where IDEliminatoriaGrupo=" + numero + ";");
        try {
            rs.next();
            String aux = rs.getString("nome da eliminatoriaGrupo");
            String state = rs.getString("estado");
            eg.setNomeGruposEliminatorias(aux);
            eg.setEstado(state);
            eg.setidGruposEliminatoria(numero);
            eg.setVencedor(rs.getString("vencedor"));
            eg.setidEquipas(Integer.parseInt(rs.getString("equipas")));
            eg.setClassificacao(Integer.parseInt(rs.getString("classificação")));
            eg.setPontuacao(Integer.parseInt(rs.getString("pontuação")));
            eg.setVitorias(Integer.parseInt(rs.getString("vitórias")));
            eg.setDerrotas(Integer.parseInt(rs.getString("derrotas")));
            eg.setEmpates(Integer.parseInt(rs.getString("empates")));
        } catch (SQLException ex) {
            System.out.println("erro get: " + ex.getStackTrace());
        }
        conn.close();
    }

    public static boolean addEliminatoriaGrupo(GruposEliminar eg) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into eliminatoriaGrupo(IDEliminatoriaGrupo,nomeEliminatoriaGrupo,estado,dataCriacao,vencedores) values(" + eg.getidGruposEliminatoria() + "," + eg.getNomeGruposEliminatorias() + ","
                + eg.getEstado() + "," + eg.getDataCriacao() + "," + eg.getVencedor() + "," + ");");
        conn.close();
        return res;
    }

    public static void showJogo(int numero, Jogos j) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select * from jogos where IDJogo=" + numero + ";");
        try {
            rs.next();
            String tip = rs.getString("tipoTorneio");
            j.setTipoTorneio(tip);
            j.setidJogo(numero);
            j.setidEquipa1(Integer.parseInt(rs.getString("equipa1")));
            j.setidEquipa2(Integer.parseInt(rs.getString("equipa2")));
            j.setResultado(rs.getString("resultado"));

        } catch (SQLException ex) {
            System.out.println("erro get: " + ex.getStackTrace());
        }
        conn.close();
    }

    public static boolean addJogo(Jogos j) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into jogos(IDJogo,IDEquipa1,IDEquipa2, IDtipoTorneio, resultado, dataHoraJogo) values(" + j.getidJogo() + "," + j.getidEquipa1() + ","
                + j.getidEquipa2() + "," + j.getTipoTorneio() + "," + j.getResultado() + "," + j.getDataHoraJogo() + ");");
        conn.close();
        return res;
    }
}
