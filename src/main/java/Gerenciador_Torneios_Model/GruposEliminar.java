/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Model;

import Gerenciador_Torneios_Data.Data;

/**
 *
 * @author utilizador
 */
public class GruposEliminar {

    private String nomeGruposEliminatorias;
    private int idGruposEliminatoria;
    private int idequipas;
    private String estado;
    private String vencedor;
    private Data dataCriacao;
    private int classificacao;
    private int pontuacao;
    private int vitorias;
    private int derrotas;
    private int empates;
    private String resultado;
    private Data dataHoraJogo;

    private static final String NOME_GRUPOS_ELIMINATORIAS_POR_OMISSAO = "Sem nome";
    private static final int IDGRUPOS_ELIMINATORIAS_POR_OMISSAO = 0;
    private static final int IDEQUIPAS_POR_OMISSAO = 0;
    private static final String ESTADO_POR_OMISSAO = "";
    private static final String VENCEDOR_POR_OMISSAO = "";
    private static final int CLASSIFICACAO_POR_OMISSAO = 0;
    private static final int PONTUACAO_POR_OMISSAO = 0;
    private static final int VITORIAS_POR_OMISSAO = 0;
    private static final int DERROTAS_POR_OMISSAO = 0;
    private static final int EMPATES_POR_OMISSAO = 0;
    private static final String RESULTADO_POR_OMISSAO = "Não disponível";

    public GruposEliminar() {
        this.nomeGruposEliminatorias = NOME_GRUPOS_ELIMINATORIAS_POR_OMISSAO;
        this.idGruposEliminatoria = IDGRUPOS_ELIMINATORIAS_POR_OMISSAO;
        this.idequipas = IDEQUIPAS_POR_OMISSAO;
        this.estado = ESTADO_POR_OMISSAO;
        this.vencedor = VENCEDOR_POR_OMISSAO;
        this.classificacao = CLASSIFICACAO_POR_OMISSAO;
        this.pontuacao = PONTUACAO_POR_OMISSAO;
        this.vitorias = VITORIAS_POR_OMISSAO;
        this.derrotas = DERROTAS_POR_OMISSAO;
        this.empates = EMPATES_POR_OMISSAO;
        this.resultado = RESULTADO_POR_OMISSAO;
    }

    public GruposEliminar(String nomeGruposEliminatorias, int idGruposEliminatoria, int idequipas, Data dataCriacao, String estado, String vencedor, int classificacao, int pontuacao, int vitorias, int derrotas, int empates, String resultado, Data dataHoraJogo) {
        this.nomeGruposEliminatorias = nomeGruposEliminatorias;
        this.idGruposEliminatoria = idGruposEliminatoria;
        this.idequipas = idequipas;
        this.dataCriacao = dataCriacao;
        this.estado = estado;
        this.classificacao = classificacao;
        this.vencedor = vencedor;
        this.pontuacao = pontuacao;
        this.vitorias = vitorias;
        this.derrotas = derrotas;
        this.empates = empates;
        this.resultado = resultado;
        this.dataHoraJogo = dataHoraJogo;
    }

    public String getNomeGruposEliminatorias() {
        return this.nomeGruposEliminatorias;
    }

    public void setNomeGruposEliminatorias(String nomeGruposEliminatorias) {
        this.nomeGruposEliminatorias = nomeGruposEliminatorias;
    }

    public int getidGruposEliminatoria() {
        return this.idGruposEliminatoria;
    }

    public void setidGruposEliminatoria(int idGruposEliminatoria) {
        this.idGruposEliminatoria = idGruposEliminatoria;
    }

    public int getidEquipas() {
        return this.idequipas;
    }

    public void setidEquipas(int idequipas) {
        this.idequipas = idequipas;
    }

    public Data getDataCriacao() {
        return this.dataCriacao;
    }

    public void setDataCriacao(Data dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getVencedor() {
        return this.vencedor;
    }

    public void setVencedor(String vencedor) {
        this.vencedor = vencedor;
    }

    public int getClassificacao() {
        return this.classificacao;
    }

    public void setClassificacao(int classificacao) {
        this.classificacao = classificacao;
    }

    public int getPontuacao() {
        return this.pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public int getVitorias() {
        return this.vitorias;
    }

    public void setVitorias(int vitorias) {
        this.vitorias = vitorias;
    }

    public int getDerrotas() {
        return this.derrotas;
    }

    public void setDerrotas(int derrotas) {
        this.derrotas = derrotas;
    }

    public int getEmpates() {
        return this.empates;
    }

    public void setEmpates(int empates) {
        this.empates = empates;
    }

    public String getResultado() {
        return this.resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Data getDataHoraJogo() {
        return this.dataHoraJogo;
    }

    public void setDataHoraJogo(Data dataHoraJogo) {
        this.dataHoraJogo = dataHoraJogo;
    }

}
