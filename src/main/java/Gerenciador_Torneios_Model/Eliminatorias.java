/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Model;

import Gerenciador_Torneios_Data.Data;

/**
 *
 * @author utilizador
 */
public class Eliminatorias {

    private String nomeEliminatoria;
    public int numero;
    private String estado;
    private Data dataCriacaoEliminatoria;
    private int idequipas;
    private Data dataHoraJogoEliminatoria;
    private String resultado;
    private String vencedor;

    private static final String NOME_ELIMINATORIA_POR_OMISSAO = "";
    private static final int NUMERO_POR_OMISSAO = 0;
    private static final String ESTADO_POR_OMISSAO = "";
    private static final int IDEQUIPAS_POR_OMISSAO = 0;
    private static final String RESULTADO_POR_OMISSAO = "Não disponivel";
    private static final String VENCEDOR_POR_OMISSAO = "";

    public Eliminatorias() {
        this.nomeEliminatoria = NOME_ELIMINATORIA_POR_OMISSAO;
        this.estado = ESTADO_POR_OMISSAO;
        this.numero = NUMERO_POR_OMISSAO;
        this.idequipas = IDEQUIPAS_POR_OMISSAO;
        this.resultado = RESULTADO_POR_OMISSAO;
        this.vencedor = VENCEDOR_POR_OMISSAO;
    }

    public Eliminatorias(String nomeEliminatoria, int numero, String estado, Data dataCriacaoEliminatoria, int idequipas, Data dataHoraJogoEliminatoria, String resultado, String vencedor) {
        this.nomeEliminatoria = nomeEliminatoria;
        this.estado = estado;
        this.numero = numero;
        this.dataCriacaoEliminatoria = dataCriacaoEliminatoria;
        this.idequipas = idequipas;
        this.dataHoraJogoEliminatoria = dataHoraJogoEliminatoria;
        this.resultado = resultado;
        this.vencedor = vencedor;
    }

    public String getNomeEliminatoria() {
        return this.nomeEliminatoria;
    }

    public void setNomeEliminatoria(String nomeEliminatoria) {
        this.nomeEliminatoria = nomeEliminatoria;
    }

    public int getNumeroEliminatoria() {
        return this.numero;
    }

    public void setNumeroEliminatoria(int numero) {
        this.numero = numero;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Data getDataCriacaoEliminatoria() {
        return this.dataCriacaoEliminatoria;
    }

    public void setDataCriacaoEliminatoria(Data dataCriacaoEliminatoria) {
        this.dataCriacaoEliminatoria = dataCriacaoEliminatoria;
    }

    public int getidEquipas() {
        return this.idequipas;
    }

    public void setidEquipas(int idequipas) {
        this.idequipas = idequipas;
    }

    public Data getdataHoraJogoEliminatoria() {
        return this.dataHoraJogoEliminatoria;
    }

    public void setDataHoraJogoEliminatoria(Data dataHoraJogoEliminatoria) {
        this.dataHoraJogoEliminatoria = dataHoraJogoEliminatoria;
    }

    public String getResultado() {
        return this.resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getVencedor() {
        return this.vencedor;
    }

    public void setVencedor(String vencedor) {
        this.vencedor = vencedor;
    }
}
