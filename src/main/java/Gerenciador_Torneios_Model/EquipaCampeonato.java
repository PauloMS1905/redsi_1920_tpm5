/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Model;

/**
 *
 * @author utilizador
 */
public class EquipaCampeonato {

    private int idCampeonato;
    private int idEquipa;
    private int classificacao;
    private int pontuacao;
    private int vitorias;
    private int derrotas;
    private int empates;

    public EquipaCampeonato() {
    }

    public EquipaCampeonato(int idCampeonato, int idEquipa, int classificacao, int pontuacao, int vitorias, int derrotas, int empates) {
        this.idCampeonato = idCampeonato;
        this.idEquipa = idEquipa;
        this.classificacao = classificacao;
        this.pontuacao = pontuacao;
        this.vitorias = vitorias;
        this.derrotas = derrotas;
        this.empates = empates;
    }

    public int getidEquipa() {
        return this.idEquipa;
    }

    public void setidEquipa(int idEquipa) {
        this.idEquipa = idEquipa;
    }

    public int getidCampeonato() {
        return this.idCampeonato;
    }

    public void setidCampeonato(int idCampeonato) {
        this.idCampeonato = idCampeonato;
    }

    public int getClassificacao() {
        return this.classificacao;
    }

    public void setClassificacao(int classificacao) {
        this.classificacao = classificacao;
    }

    public int getPontuacao() {
        return this.pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public int getVitorias() {
        return this.vitorias;
    }

    public void setVitorias(int vitorias) {
        this.vitorias = vitorias;
    }

    public int getDerrotas() {
        return this.derrotas;
    }

    public void setDerrotas(int derrotas) {
        this.derrotas = derrotas;
    }

    public int getEmpates() {
        return this.empates;
    }

    public void setEmpates(int empates) {
        this.empates = empates;
    }
}
