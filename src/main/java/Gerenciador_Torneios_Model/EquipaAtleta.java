/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Model;

/**
 *
 * @author utilizador
 */
public class EquipaAtleta {

    private int idEquipa;
    private int idAtleta;

    public EquipaAtleta() {
    }

    public EquipaAtleta(int idEquipa, int idAtleta) {
        this.idEquipa = idEquipa;
        this.idAtleta = idAtleta;
    }

    public int getidAtleta() {
        return this.idAtleta;
    }

    public int getidEquipa() {
        return this.idEquipa;
    }

    public void setidAtleta(int idAtleta) {
        this.idAtleta = idAtleta;
    }

    public void setidEquipa(int idEquipa) {
        this.idEquipa = idEquipa;
    }

}
