/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Model;

import Gerenciador_Torneios_Data.Data;

/**
 *
 * @author utilizador
 */
public class Campeonato {

    private String nomeCampeonato;
    private int numero;
    private int idequipas;
    private Data dataCriacaoCampeonato;
    private String estado;
    private String vencedores;
    private int classificacao;
    private int pontuacao;
    private int vitorias;
    private int derrotas;
    private int empates;

    private static final String NOME_CAMPEONATO_POR_OMISSAO = "";
    private static final int NUMERO_POR_OMISSAO = 0;
    private static final int EQUIPAS_POR_OMISSAO = 0;
    private static final String ESTADO_POR_OMISSAO = "Não especificado";
    private static final String VENCEDORES_POR_OMISSAO = "Por definir";
    private static final int CLASSIFICACAO_POR_OMISSAO = 0;
    private static final int PONTUACAO_POR_OMISSAO = 0;
    private static final int VITORIAS_POR_OMISSAO = 0;
    private static final int DERROTAS_POR_OMISSAO = 0;
    private static final int EMPATES_POR_OMISSAO = 0;

    public Campeonato() {
        this.nomeCampeonato = NOME_CAMPEONATO_POR_OMISSAO;
        this.numero = NUMERO_POR_OMISSAO;
        this.idequipas = EQUIPAS_POR_OMISSAO;
        this.estado = ESTADO_POR_OMISSAO;
        this.vencedores = VENCEDORES_POR_OMISSAO;
        this.classificacao = CLASSIFICACAO_POR_OMISSAO;
        this.pontuacao = PONTUACAO_POR_OMISSAO;
        this.vitorias = VITORIAS_POR_OMISSAO;
        this.derrotas = DERROTAS_POR_OMISSAO;
        this.empates = EMPATES_POR_OMISSAO;
    }

    public Campeonato(String nomeCampeonato, int numero, int idequipas, Data dataCriacaoCampeonato, String estado, String vencedores, int classificacao, int pontuacao, int vitorias, int derrotas, int empates) {
        this.nomeCampeonato = nomeCampeonato;
        this.numero = numero;
        this.idequipas = idequipas;
        this.dataCriacaoCampeonato = dataCriacaoCampeonato;
        this.estado = estado;
        this.vencedores = vencedores;
        this.classificacao = classificacao;
        this.pontuacao = pontuacao;
        this.vitorias = vitorias;
        this.derrotas = derrotas;
        this.empates = empates;
    }

    public String getNomeCampeonato() {
        return this.nomeCampeonato;
    }

    public void setNomeCampeonato(String nomeCampeonato) {
        this.nomeCampeonato = nomeCampeonato;
    }

    public int getNumeroCampeonato() {
        return this.numero;
    }

    public void setNumeroCampeonato(int numeroCampeonato) {
        this.numero = numero;
    }

    public int getidEquipas() {
        return this.idequipas;
    }

    public void setidEquipas(int idequipas) {
        this.idequipas = idequipas;
    }

    public Data getDataCriacaoCampeonato() {
        return this.dataCriacaoCampeonato;
    }

    public void setDataCriacaoCampeonato(Data dataCriacaoCampeonato) {
        this.dataCriacaoCampeonato = dataCriacaoCampeonato;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getVencedores() {
        return this.vencedores;
    }

    public void setVencedores(String vencedores) {
        this.vencedores = vencedores;
    }

    public int getPontuacao() {
        return this.pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public int getClassificacao() {
        return this.classificacao;
    }

    public void setClassificacao(int classificacao) {
        this.classificacao = classificacao;
    }

    public int getVitorias() {
        return this.vitorias;
    }

    public void setVitorias(int vitorias) {
        this.vitorias = vitorias;
    }

    public int getDerrotas() {
        return this.derrotas;
    }

    public void setDerrotas(int derrotas) {
        this.derrotas = derrotas;
    }

    public int getEmpates() {
        return this.empates;
    }

    public void setEmpates(int empates) {
        this.empates = empates;
    }
}
