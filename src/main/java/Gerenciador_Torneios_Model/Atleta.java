/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Model;

/**
 *
 * @author utilizador
 */
public class Atleta {

    private String nomeAtleta;
    private String nacionalidade;
    private int idade;
    private int idAtleta;
    private String curiosidadesAtleta;

    private static final String NOME_ATLETA_POR_OMISSAO = "";
    private static final String NACIONALIDADE_POR_OMISSAO = "Não definida";
    private static final int IDADE_POR_OMISSAO = 0;
    private static final int NUMERO_IDENTIFICACAO_ATLETA_POR_OMISSAO = 0;
    private static final String CURIOSIDADES_ATLETA_POR_OMISSAO = "Não foram dadas informações adicionais";

    public Atleta() {
        this.nomeAtleta = NOME_ATLETA_POR_OMISSAO;
        this.nacionalidade = NACIONALIDADE_POR_OMISSAO;
        this.idade = IDADE_POR_OMISSAO;
        this.idAtleta = NUMERO_IDENTIFICACAO_ATLETA_POR_OMISSAO;
        this.curiosidadesAtleta = CURIOSIDADES_ATLETA_POR_OMISSAO;
    }

    public Atleta(String nomeAtleta, String nacionalidade, int idade, int idAtleta, String curiosidadesAtleta) {
        this.nomeAtleta = nomeAtleta;
        this.nacionalidade = nacionalidade;
        this.idade = idade;
        this.idAtleta = idAtleta;
        this.curiosidadesAtleta = curiosidadesAtleta;
    }

    public String getNomeAtleta() {
        return this.nomeAtleta;
    }

    public void setNomeAtleta(String nomeAtleta) {
        this.nomeAtleta = nomeAtleta;
    }

    public String getNacionalidade() {
        return this.nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public int getIdade() {
        return this.idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getNumeroIdentifacaoAtleta() {
        return this.idAtleta;
    }

    public void setNumeroIdentificacaoAtleta(int idAtleta) {
        this.idAtleta = idAtleta;
    }

    public String getCuriosidadesAtleta() {
        return this.curiosidadesAtleta;
    }

    public void setCuriosidadesAtleta(String curiosidadesAtleta) {
        this.curiosidadesAtleta = curiosidadesAtleta;
    }
}
