/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Model;

/**
 *
 * @author utilizador
 */
public class EquipaEliminatoria {

    private int idEliminatoria;
    private int idEquipa;

    public EquipaEliminatoria() {
    }

    public EquipaEliminatoria(int idEliminatoria, int idEquipa) {
        this.idEliminatoria = idEliminatoria;
        this.idEquipa = idEquipa;
    }

    public int getidEquipa() {
        return this.idEquipa;
    }

    public int getidEliminatoria() {
        return this.idEliminatoria;
    }

    public void setidEquipa(int idEquipa) {
        this.idEquipa = idEquipa;
    }

    public void setidEliminatoria(int idEliminatoria) {
        this.idEliminatoria = idEliminatoria;
    }

}
