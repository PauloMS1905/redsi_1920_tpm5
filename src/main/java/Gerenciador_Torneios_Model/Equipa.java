/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Model;

import Gerenciador_Torneios_Data.Data;

/**
 *
 * @author utilizador
 */
public class Equipa {

    private String nomeEquipa;
    private int numero;
    private String modalidade;
    private String treinador;
    private String recintoDesportivo;
    private Data dataConstrucao;
    private int lotacao;
    private String curiosidadesEquipa;

    private static final String NOME_EQUIPA_POR_OMISSAO = "Por definir";
    private static final int NUMERO_POR_OMISSAO = 0;
    private static final String MODALIDADE_POR_OMISSAO = "Não especificada";
    private static final String TREINADOR_POR_OMISSAO = "Sem treinador";
    private static final String RECINTO_DESPORTIVO_POR_OMISSAO = "Sem recinto desportivo próprio";
    private static final int LOTACAO_POR_OMISSAO = 0;
    private static final String CURIOSIDADES_EQUIPA_POR_OMISSAO = "Não foram dadas informações adicionais";

    public Equipa() {
        this.nomeEquipa = NOME_EQUIPA_POR_OMISSAO;
        this.numero = NUMERO_POR_OMISSAO;
        this.modalidade = MODALIDADE_POR_OMISSAO;
        this.treinador = TREINADOR_POR_OMISSAO;
        this.recintoDesportivo = RECINTO_DESPORTIVO_POR_OMISSAO;
        this.lotacao = LOTACAO_POR_OMISSAO;
        this.curiosidadesEquipa = CURIOSIDADES_EQUIPA_POR_OMISSAO;
    }

    public Equipa(String nomeEquipa, int numero, String modalidade, String treinador, String recintoDesportivo, Data dataConstrucao, int lotacao, String curiosidadesEquipa) {
        this.nomeEquipa = nomeEquipa;
        this.numero = numero;
        this.modalidade = modalidade;
        this.treinador = treinador;
        this.recintoDesportivo = recintoDesportivo;
        this.dataConstrucao = dataConstrucao;
        this.lotacao = lotacao;
        this.curiosidadesEquipa = curiosidadesEquipa;
    }

    public String getNomeEquipa() {
        return this.nomeEquipa;
    }

    public void setNomeEquipa(String nomeEquipa) {
        this.nomeEquipa = nomeEquipa;
    }

    public int getNumero() {
        return this.numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getModalidade() {
        return this.modalidade;
    }

    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }

    public String getTreinador() {
        return this.treinador;
    }

    public void setTreinador(String treinador) {
        this.treinador = treinador;
    }

    public String getRecintoDesportivo() {
        return this.recintoDesportivo;
    }

    public void setRecintoDesportivo(String recintoDesportivo) {
        this.recintoDesportivo = recintoDesportivo;
    }

    public Data getDataContrucao() {
        return this.dataConstrucao;
    }

    public void setDataConstrucao(Data dataConstrucao) {
        this.dataConstrucao = dataConstrucao;
    }

    public int getLotacao() {
        return this.lotacao;
    }

    public void setLotacao(int lotacao) {
        this.lotacao = lotacao;
    }

    public String getCuriosidadesEquipas() {
        return this.curiosidadesEquipa;
    }

    public void setCuriosidadesEquipas(String curiosidadesEquipas) {
        this.curiosidadesEquipa = curiosidadesEquipas;
    }

}
