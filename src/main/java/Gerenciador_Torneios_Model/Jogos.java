/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Model;

import Gerenciador_Torneios_Data.Data;

/**
 *
 * @author utilizador
 */
public class Jogos {

    private int idJogo;
    private String tipoTorneio;
    private int idequipa1;
    private int idequipa2;
    private String resultado;
    private Data dataHoraJogo;

    private static final int ID_JOGO_POR_OMISSAO = 0;
    private static final String TIPO_TORNEIO_POR_OMISSAO = "";
    private static final int ID_EQUIPA1_POR_OMISSAO = 0;
    private static final int ID_EQUIPA2_POR_OMISSAO = 0;
    private static final String RESULTADO_POR_OMISSAO = "Resultado indisponivel";

    public Jogos() {
        this.idJogo = ID_JOGO_POR_OMISSAO;
        this.tipoTorneio = TIPO_TORNEIO_POR_OMISSAO;
        this.idequipa1 = ID_EQUIPA1_POR_OMISSAO;
        this.idequipa2 = ID_EQUIPA2_POR_OMISSAO;
        this.resultado = RESULTADO_POR_OMISSAO;
    }

    public Jogos(int idJogo, String tipoTorneio, int idequipa1, int idequipa2, String resultado, Data dataHoraJogo) {
        this.idJogo = idJogo;
        this.tipoTorneio = tipoTorneio;
        this.idequipa1 = idequipa1;
        this.idequipa2 = idequipa2;
        this.resultado = resultado;
        this.dataHoraJogo = dataHoraJogo;
    }

    public int getidJogo() {
        return this.idJogo;
    }

    public void setidJogo(int idJogo) {
        this.idJogo = idJogo;
    }

    public String getTipoTorneio() {
        return this.tipoTorneio;
    }

    public void setTipoTorneio(String tipoTorneio) {
        this.tipoTorneio = tipoTorneio;
    }

    public int getidEquipa1() {
        return this.idequipa1;
    }

    public void setidEquipa1(int idequipa1) {
        this.idequipa1 = idequipa1;
    }

    public int getidEquipa2() {
        return this.idequipa2;
    }

    public void setidEquipa2(int idequipa2) {
        this.idequipa2 = idequipa2;
    }

    public String getResultado() {
        return this.resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Data getDataHoraJogo() {
        return this.dataHoraJogo;
    }

    public void setDataHoraJogo(Data dataHoraJogo) {
        this.dataHoraJogo = dataHoraJogo;
    }
}
