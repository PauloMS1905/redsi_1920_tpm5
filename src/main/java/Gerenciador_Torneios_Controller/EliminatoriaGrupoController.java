/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Controller;

import Gerenciador_Torneios_DB.DAL;
import Gerenciador_Torneios_Model.GruposEliminar;
import java.sql.SQLException;

/**
 *
 * @author utilizador
 */
public class EliminatoriaGrupoController {

    public static void mostrarEliminatoriaGrupos(int numero, GruposEliminar eg) throws SQLException {
        DAL.showEliminatoriaGrupo(numero, eg);
    }

    public static void guardarEliminatoriaGrupo(GruposEliminar eg) throws SQLException {
        DAL.addEliminatoriaGrupo(eg);
    }
}
