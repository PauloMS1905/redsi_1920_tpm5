/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Controller;

import Gerenciador_Torneios_DB.DAL;
import Gerenciador_Torneios_Model.Campeonato;
import java.sql.SQLException;

/**
 *
 * @author utilizador
 */
public class CampeonatoController {

    public static void mostrarCampeonato(int numero, Campeonato c) throws SQLException {
        DAL.showCampeonato(numero, c);
    }

    public static void guardarCampeonato(Campeonato c) throws SQLException {
        DAL.addCampeonato(c);
    }
}
