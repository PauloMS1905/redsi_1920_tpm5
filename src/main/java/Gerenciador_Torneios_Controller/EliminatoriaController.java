/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Controller;

import Gerenciador_Torneios_DB.DAL;
import Gerenciador_Torneios_Model.Eliminatorias;
import java.sql.SQLException;

/**
 *
 * @author utilizador
 */
public class EliminatoriaController {

    public static void mostrarEliminatoria(int numero, Eliminatorias e) throws SQLException {
        DAL.showEliminatoria(numero, e);
    }

    public static void guardarEliminatoria(Eliminatorias e) throws SQLException {
        DAL.addEliminatoria(e);
    }
}
