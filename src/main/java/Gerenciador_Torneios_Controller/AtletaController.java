/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Controller;

import Gerenciador_Torneios_DB.DAL;
import Gerenciador_Torneios_Model.Atleta;
import java.sql.SQLException;

/**
 *
 * @author utilizador
 */
public class AtletaController {

    public static void guardarAtleta(Atleta a) throws SQLException {
        DAL.addAtleta(a);
    }

    public static void mostrarAtleta(int numero, Atleta a) throws SQLException {
        DAL.showAtleta(numero, a);
    }

}
