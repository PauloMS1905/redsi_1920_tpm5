/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Controller;

import Gerenciador_Torneios_DB.DAL;
import Gerenciador_Torneios_Model.Jogos;
import java.sql.SQLException;

/**
 *
 * @author utilizador
 */
public class JogosController {

    public static void guardarJogo(Jogos j) throws SQLException {
        DAL.addJogo(j);
    }

    public static void mostrarJogo(int numero, Jogos j) throws SQLException {
        DAL.showJogo(numero, j);
    }

}
