/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Controller;

import Gerenciador_Torneios_DB.DAL;
import Gerenciador_Torneios_Model.EquipaCampeonato;
import java.sql.SQLException;

/**
 *
 * @author utilizador
 */
public class EquipaCampeonatoController {

    public static void associarEquipaCampeonato(EquipaCampeonato c) throws SQLException {
        DAL.addEquipaCampeonato(c);
    }
}
