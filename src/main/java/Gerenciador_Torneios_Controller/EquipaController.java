/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Controller;

import Gerenciador_Torneios_DB.DAL;
import Gerenciador_Torneios_Model.Equipa;
import Gerenciador_Torneios_Model.EquipaCampeonato;
import Gerenciador_Torneios_Model.EquipaEliminatoria;
import Gerenciador_Torneios_Model.EquipaEliminatoriaGrupo;
import java.sql.SQLException;

/**
 *
 * @author utilizador
 */
public class EquipaController {

    public static void guardarEquipa(Equipa e) throws SQLException {
        DAL.addEquipa(e);
    }

    public static void mostrarEquipa(int numero, Equipa e) throws SQLException {
        DAL.showEquipa(numero, e);
    }

}
