/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerenciador_Torneios_Controller;

import Gerenciador_Torneios_DB.DAL;
import Gerenciador_Torneios_Model.EquipaEliminatoriaGrupo;
import java.sql.SQLException;

/**
 *
 * @author utilizador
 */
public class EquipaEliminatoriaGrupoController {
    
    public static void associarEquipaEliminatoriaGrupo(EquipaEliminatoriaGrupo eg) throws SQLException {
        DAL.addEquipaEliminatoriaGrupo(eg);
    }
    
}
